module.exports = {

  port: process.env.PORT || 7835,
  environment: process.env.NODE_ENV || 'development'

};
