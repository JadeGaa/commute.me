module.exports.connections = {

  localDiskDb: {
    adapter: 'sails-disk'
  },

  commuteSql: {
    adapter: 'sails-mysql',
    host: 'localhost',
    user: 'commute',
    password: 'commute',
    database: 'commute'
  }

};
