module.exports.routes = {
  'get /v1/routes': 'v1/Journeys.getRecommended',
  'get /v1/points': 'v1/Points.getAll',

  'post /segments/add' : 'SegmentsController.addSegments',
  'post /routes/add' : 'RoutesController.addRoutes',
  'post /routeOptions/add' : 'RouteOptionsController.addRouteOptions',
  'post /routeOptions/:routeOptionId/rate' : 'RatingsController.rateRouteOption'
};
