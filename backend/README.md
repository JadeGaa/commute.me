Because commuting sucks.

### Local Environment Setup

* Install modules

```sh
$ npm install
```

* Setup local MySQL database

```sh
$ mysql -u root
```
```sql
CREATE DATABASE `commute`;
GRANT ALL ON commute.* TO 'commute'@'localhost' IDENTIFIED BY 'commute'; 
```

* Start the app

```sh
$ node app.js
```
