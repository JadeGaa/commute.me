
module.exports = {

  attributes: {

    segmentId: {
      type: 'integer',
    },

    time: {
      type: 'float'
    },

    cost: {
      type: 'float'
    },

    safety: {
      type: 'float'
    },

    userId: {
      type: 'string'
    },

    timeId:{
      type: 'integer'
    }

  }
};
