module.exports = {

  attributes: {
    dayOfWeek: {
      type: 'string'
    },

    startHour: {
      type: 'string'
    },

    endHour: {
      type: 'string'
    }
  }
};

