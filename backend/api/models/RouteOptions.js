module.exports = {
  attributes: {

    startLat: {
      type: 'string'
    },

    startLong: {
      type: 'string'
    },

    endLat: {
      type: 'string'
    },

    endLong: {
      type: 'string'
    },

    ETA: {
      type: 'integer',
      defaultsTo: 60
    },

    estimatedCost: {
      type: 'integer',
      defaultsTo: 30
    }
  }
};
