/**
* Segments.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    startLat: {
      type: 'string'
    },

    startLong: {
      type: 'string'
    },

    endLat: {
      type: 'string'
    },

    endLong: {
      type: 'string'
    },

    mode: {
      type: 'string'
    }

  }
};

