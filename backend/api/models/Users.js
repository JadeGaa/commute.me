module.exports = {

  attributes: {
    userId: {
      type: 'string',
      unique: true
    },

    firstName: {
      type: 'string'
    },

    lastName: {
      type: 'string'
    }
  }
};

