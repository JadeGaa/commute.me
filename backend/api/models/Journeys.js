module.exports = {

  attributes: {

    userId: {
      type: 'string'
    },

    routeId: {
      type: 'integer'
    }
  }

};

