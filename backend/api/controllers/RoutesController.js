/**
 * RoutesController
 *
 * @description :: Server-side logic for managing Routes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

	addRoutes: function(req, res) {
    async.waterfall([
      function(callback) {
        var returnObjects = [];
        
        req.body.routes.forEach(function(route) {
          Routes.create(route, function onCreate(err, route) {
            if (err) {
              return callback(err);
            }
            else {
              returnObjects.push(route);
            }
          });
        });

        callback(null, returnObjects);    
      }
    ], function(err, routes) {
        if(err) {
          res.json(err);
        }
        else {
          res.json(routes);
        }
    });
  }

};

