/**
 * RatingsController
 *
 * @description :: Server-side logic for managing Ratings
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
  rateRouteOption: function(req, res) {
    var routeOptionId = req.param.routeOptionId;
    async.waterfall([
      function(callback) {
        Routes.find({"routeOptionId" : routeOptionId}, function onFind(err, result) {
          if (err) {
            return callback(err);
          }
          else {
            callback(null, result);
          }
        })
      },

      function(result, callback) {
        var segmentIds = _.map(result.get("segmentId"), function(segment) {
            return segment;
        });

        async.each(segmentIds, function( segmentId, cb) {

          Ratings.create({
            "segmentId": segmentId,
            "time": req.body.time,
            "cost": req.body.cost,
            "safety": req.body.safety,
            "userId": req.body.userId
          }, function onCreate(err, rating) {
            if (err) {
              return(err);
            }
            cb();
          });

        }, function(err){
          callback();
        });

      }

    ], function(err, result) {
      if (err) {
        return res.json(err);
      }
      else {
        return res.json("OK");
      }
    });

  }

};

