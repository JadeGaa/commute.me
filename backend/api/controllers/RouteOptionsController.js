/**
 * RouteOptionsController
 *
 * @description :: Server-side logic for managing Routeoptions
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
  addRouteOptions: function(req, res) {
    async.waterfall([
      function(callback) {
        var returnObjects = [];
        
        req.body.routeOptions.forEach(function(routeOption) {
          RouteOptions.create(routeOption, function onCreate(err, routeOption) {
            if (err) {
              return callback(err);
            }
            else {
              returnObjects.push(routeOption);
            }
          });
        });

        callback(null, returnObjects);    
      }
    ], function(err, routeOptions) {
        if(err) {
          res.json(err);
        }
        else {
          res.json(routeOptions);
        }
    });
  }
  
};

