/**
 * SegmentsController
 *
 * @description :: Server-side logic for managing Segments
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
  addSegments: function(req, res) {
    async.waterfall([
      function(callback) {
        var returnObjects = [];
        
        req.body.segments.forEach(function(segment) {
          Segments.create(segment, function onCreate(err, segment) {
            if (err) {
              return callback(err);
            }
            else {
              returnObjects.push(segment);
            }
          });
        });

        callback(null, returnObjects);    
      }
    ], function(err, segments) {
        if(err) {
          res.json(err);
        }
        else {
          res.json(segments);
        }
    });
  }

};

