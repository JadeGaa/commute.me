module.exports = {
  getAll: function(req, res) {
    var points = [];
    RouteOptions.find(function(err, segments) {
      if (err) return res.json(500, { error: err });

      points = [{
        startLat: segments[0].startLat,
        startLong: segments[0].startLong,
        endLat: segments[0].endLat,
        endLong: segments[0].endLong
      }];

      res.json({ points: points });
    });
  }
};
