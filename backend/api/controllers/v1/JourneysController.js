module.exports = {
  getRecommended: function(req, res) {

    async.auto({ 
      routeOptions: function(callback) {
        RouteOptions.find({ 
          startLong: parseFloat(req.query.startLong),
          startLat: parseFloat(req.query.startLat),
          endLong: parseFloat(req.query.endLong),
          endLat: parseFloat(req.query.endLat) 
        }, function(err, routes) {
          if (err) return callback(err);
          if (!routes) return callback('no_routes'); 
          if (_.isEmpty(routes)) return callback('empty_routes'); 

          callback(null, routes); 
        });
      },

      routes: ['routeOptions', function(callback, result) {
        var routeIds = _.map(result.routeOptions, function(route) {
          return route.id;
        });

        Routes.find({ routeOptionId: routeIds }, function(err, routes) {
          if (err) return callback(err);
          if (!routes) return callback('no_route_options'); 
          if (_.isEmpty(routes)) return callback('empty_route_options'); 

          callback(null, routes);
        });
      }],

      segments: ['routes', function(callback, result) {
        var segmentData = _.groupBy(result.routes, function(route) {
          return route.routeOptionId;
        });

        var journeys = [];
        var routeSeg = [];
        
        _.each(segmentData, function(segments, routeId) {
          routeSeg.push({
            segments: segments,
            routeId: routeId
          });
        });

        async.each(routeSeg, function(rs, rCallback) {
          var segments = rs.segments;
          var routeId = rs.routeId;
          var segs = [];
          var rates = 0;
          var ratings = {
            speed: 0,
            safety: 0,
            cost: 0
          };

          async.each(segments, function(segment, eCallback) {
            Segments.findOne({ id: segment.segmentId }, function(err, seg) {
              if (err) return eCallback(err);

              segs.push({
                startLat: seg.startLat,
                startLong: seg.startLong,
                endLat: seg.endLat,
                endLong: seg.endLong,
                mode: seg.mode
              });

              Ratings.findOne({ segmentId: seg.id }, function(err, rating) {
                if (rating) {
                  ratings.speed += rating.speed;
                  ratings.safety += rating.safety;
                  ratings.cost += rating.cost;
                  rates += 1;
                }
                
                eCallback();
              });

            });
          }, function(err) {
            if (err) return rCallback(err);

            if (rates > 0) {
              ratings.speed = ratings.speed / rates;
              ratings.cost = ratings.cost / rates;
              ratings.safety = ratings.safety / rates;
            }
            
            var route = _.find(result.routeOptions, function(ro) {
              return ro.id == routeId;
            });

            journeys.push({
              journeyId: routeId,
              routes: segs,
              speed: ratings.speed,
              cost: ratings.cost,
              safety: ratings.safety,
              ETA: route.ETA,
              estimatedCost: route.estimatedCost
            });

            rCallback();
          });
        }, function(err) {
          if (err) return callback(err);
          
          callback(null, journeys);
        });

      }]
    }, function(err, result) {
      if (err) {
        sails.log.error(err);
        return res.json(500, { error: err });
      }

      res.json({ journeys: result.segments });
    });
    
  }
};
